
public class Somador {
	private String nome;
	private int valorInstancia = 0;
	private static int valorEstatica = 0;
	
	public Somador (String nome) {
		this.nome = nome;
	}
	
	public void somar() {
		this.valorInstancia++;
		Somador.valorEstatica++;
	}
	
	public void imprimir() {
		System.out.println("Nome: " + nome);
		System.out.println("Instancia: " + this.valorInstancia);
		System.out.println("Estatica: " + Somador.valorEstatica);
		System.out.println("--------");
	}
}
