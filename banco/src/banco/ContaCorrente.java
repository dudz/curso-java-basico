package banco;

public class ContaCorrente {

	private int saldo;
	
	public int sacar (int valor) {
		if(valor > this.saldo) {
			return 0;
		} else {
			this.saldo -= valor;
			return valor;
		}
	}
	
	public void depositar(int valor) {
		this.saldo += valor;
	}

	public int getSaldo() {
		return this.saldo;
	}
	
	public void atualizarValor(int delta) {
		this.saldo += delta;
	}
}
