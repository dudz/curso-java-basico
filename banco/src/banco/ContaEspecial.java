package banco;

public class ContaEspecial extends ContaCorrente {

	private int limite;

	ContaEspecial(int limite) {
		this.limite = limite;
	}

	@Override
	public int sacar (int valor) {
		if(valor > this.getSaldo() + this.limite) {
			return 0;
		} else {
			atualizarValor(- valor);
			return valor;
		}
	}
}
