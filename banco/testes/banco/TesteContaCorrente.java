package banco;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TesteContaCorrente {

	private ContaCorrente cc;
	
	@Before
	public void iniciarConta() {
		cc = new ContaCorrente();
	}
	
	@Test
	public void deposito() {
		cc.depositar(200);
		assertEquals(cc.getSaldo(), 200);
	}
	
	@Test
	public void saque() {
		cc.depositar(200);
		int valorSacado = cc.sacar(50);
		assertEquals(cc.getSaldo(), 150);
		assertEquals(valorSacado, 50);
	}
	
	@Test
	public void saqueMaiorQueSaldo() {
		cc.depositar(200);
		int valorSacado = cc.sacar(250);
		assertEquals(cc.getSaldo(), 200);
		assertEquals(valorSacado, 0);
	}
}
