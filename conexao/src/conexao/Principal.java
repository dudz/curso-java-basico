package conexao;

public class Principal {

	public static void main(String[] args) {

		Connection conexao = new ConexaoOracle();
		conectar(conexao);
		
		
		conexao = new ConexaoMysql();
		conectar(conexao);
	}
	
	public static void conectar (Connection conexao) {
		conexao.connect();
	}
	
}
