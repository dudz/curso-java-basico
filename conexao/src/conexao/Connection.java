package conexao;

public interface Connection {
	public abstract void rollback(); 
	public abstract void commit(); 
	public abstract void connect(); 
}
