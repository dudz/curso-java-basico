package conexao;

public class ConexaoMysql implements Connection {

	@Override
	public void rollback() {
		System.out.println("Rollback mysql");
	}

	@Override
	public void commit() {
		System.out.println("Commit mysql");
		
	}

	@Override
	public void connect() {
		System.out.println("connect mysql");
	}

}
