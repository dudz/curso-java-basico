package conexao;

public class ConexaoOracle implements Connection {

	@Override
	public void rollback() {
		System.out.println("Rollback Oracle");
	}

	@Override
	public void commit() {
		System.out.println("Commit Oracle");
		
	}

	@Override
	public void connect() {
		System.out.println("connect Oracle");
	}

}
