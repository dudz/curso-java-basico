
public class Principal {

	public static void main(String[] args) {
		Paciente[] pacientes = new Paciente[3];
		
		pacientes[0] = new Paciente(67, 1.78);
		pacientes[1] = new Paciente(80, 1.65);
		pacientes[2] = new Paciente(90, 2.04);
		
		for (Paciente p : pacientes) {
			System.out.println("IMC: " + p.calcularImc() + ": " + p.diagnostico());
		}
		
	}

}
