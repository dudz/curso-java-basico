

import org.junit.Assert;
import org.junit.Test;

public class TesteRegistroDePontos {
	@Test
	public void pontosCriarTopico() {
		Usuario u = new Usuario("Eduardo");
		CalculadoraBonus calculadoraBonus = new CalculadoraBonus();
		RegistroPontos rp = new RegistroPontos(calculadoraBonus);
		rp.criarUmTopico(u);
		Assert.assertTrue(u.getPontos() == 5);
	}
	@Test
	public void pontosCriarTopicoVip() {
		Usuario u = new Usuario("Eduardo", true);
		CalculadoraBonus calculadoraBonus = new CalculadoraBonus();
		RegistroPontos rp = new RegistroPontos(calculadoraBonus);
		rp.criarUmTopico(u);
		Assert.assertTrue(u.getPontos() == 25);
	}
	@Test
	public void pontosCriarTopicoBonusDoDia() {
		Usuario u = new Usuario("Eduardo");
		CalculadoraBonus calculadoraBonus = new CalculadoraBonus(3);
		RegistroPontos rp = new RegistroPontos(calculadoraBonus);
		rp.criarUmTopico(u);
		Assert.assertTrue(u.getPontos() == 15);
	}
	@Test
	public void pontosCriarTopicoBonusDoDiaVip() {
		Usuario u = new Usuario("Eduardo", true);
		CalculadoraBonus calculadoraBonus = new CalculadoraBonus(2);
		RegistroPontos rp = new RegistroPontos(calculadoraBonus);
		rp.criarUmTopico(u);
		Assert.assertTrue(u.getPontos() == 50);
	}
}
