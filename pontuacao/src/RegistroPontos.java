

public class RegistroPontos {

	private CalculadoraBonus bonus;
	
	public RegistroPontos(CalculadoraBonus calculadoraBonus) {
		this.bonus = calculadoraBonus;
	}
	
	public void fazerUmComentario(Usuario usuario) {
		int pts;
		pts = 3;
		usuario.somaPontos(pts * bonus.bonus(usuario));
	}
	
	public void criarUmTopico(Usuario usuario) {
		int pts;
		pts = 5;
		usuario.somaPontos(pts * bonus.bonus(usuario));
	}
	
	public void darUmLike(Usuario usuario) {
		int pts;
		pts = 1;
		usuario.somaPontos(pts * bonus.bonus(usuario));
	}
	
}
