

public class CalculadoraBonus {
	private int bonusDoDia = 1;
	
	
	public CalculadoraBonus() {
	}
	public CalculadoraBonus(int bonusDoDia) {
		this.bonusDoDia = bonusDoDia;
	}
	
	public int bonus(Usuario usuario) {
		int multiplicador = bonusDoDia;
		if(usuario.isVip()) multiplicador *= 5;
		return multiplicador;
	}
}
