

public class Usuario {
	private String nome;
	private int pontos = 0;
	private boolean vip = false;
	
	public Usuario (String nome) {
		this.nome = nome;
	}
	public Usuario (String nome, boolean vip) {
		this.nome = nome;
		this.vip = vip;
	}
	
	
	public void somaPontos(int pontos) {
		this.pontos += pontos;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getPontos() {
		return pontos;
	}
	public void setPontos(int pontos) {
		this.pontos = pontos;
	}
	public boolean isVip() {
		return vip;
	}
	public void setVip(boolean vip) {
		this.vip = vip;
	}
}
