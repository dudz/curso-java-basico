package br.gov.serpro.pagamento;

public class CartaoDeDebito extends Cartao {

	public CartaoDeDebito(int quantidade) {
		super(quantidade);
	}

	@Override
	public String getTipoPagamento() {
		return "Cartão de Débito";
	}

	@Override
	public void validar() {
		
	}
	
}
