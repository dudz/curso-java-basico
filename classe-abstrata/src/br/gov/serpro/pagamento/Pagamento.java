package br.gov.serpro.pagamento;

public abstract class Pagamento {

	private int quantidade;
	
	public abstract String getTipoPagamento();
	
	public int getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade (int quantidade) {
		this.quantidade = quantidade;
	}

	public Pagamento(int quantidade) {
		this.quantidade = quantidade;
	}
	
}
