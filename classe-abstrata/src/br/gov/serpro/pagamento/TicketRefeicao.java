package br.gov.serpro.pagamento;

public class TicketRefeicao extends Ticket {

	private int saldo = 0;
	
	public TicketRefeicao(int quantidade) {
		super(quantidade);
	}

	@Override
	public int getSaldo() {
		return saldo;
	}
	
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	@Override
	public void validar() {
	}

	@Override
	public String getTipoPagamento() {
		return "Ticket Refeição";
	}

}
