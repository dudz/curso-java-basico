package br.gov.serpro.pagamento;

public class CartaoDeCredito extends Cartao {

	public CartaoDeCredito(int quantidade) {
		super(quantidade);
	}

	@Override
	public String getTipoPagamento() {
		return "Cartão de Crédito";
	}

	@Override
	public void validar() {
		
	}

	
}
