package br.gov.serpro.pagamento;

public abstract class Ticket extends Cartao {


	public Ticket(int quantidade) {
		super(quantidade);
	}

	public abstract int getSaldo();
	
	
}
