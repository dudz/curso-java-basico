package br.gov.serpro.pagamento;

public abstract class Cartao extends Pagamento {

	public Cartao(int quantidade) {
		super(quantidade);
	}

	public abstract void validar();
	
}
