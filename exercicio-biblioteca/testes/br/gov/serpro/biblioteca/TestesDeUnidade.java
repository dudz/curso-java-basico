package br.gov.serpro.biblioteca;

import static org.junit.Assert.*;
import org.junit.Test;


public class TestesDeUnidade {

	
	@Test
	public void testaRegistraUsuario() {
		Biblioteca biblioteca = new Biblioteca();
		Usuario usuario = new Usuario ("Usuario 1");
		assertFalse(biblioteca.existeUsuario(usuario));
		biblioteca.registraUsuario(usuario);
		assertTrue(biblioteca.existeUsuario(usuario));
	}
	
	
	@Test
	public void testaAdicionaLivroAoCatalogo () {
		Biblioteca biblioteca = new Biblioteca();
		Livro livro = new Livro ("Livro 1");
		assertFalse(biblioteca.existeLivroNoCatalogo(livro));
		biblioteca.adicionaLivroAoCatalogo(livro);;
		assertTrue(biblioteca.existeLivroNoCatalogo(livro));
	}
	
	@Test
	public void testaEmprestaLivro () {
		Biblioteca biblioteca = new Biblioteca();
		Livro livro = new Livro("Livro 1");
		Usuario usuario = new Usuario ("Usuario 1");
		assertTrue(livro.estaDisponivel());
		assertFalse(livro.estaEmprestado());
		biblioteca.emprestaLivro(usuario, livro);
		assertFalse(livro.estaDisponivel());
		assertTrue(livro.estaEmprestado());
	}
	
	@Test
	public void testaDevolveLivro () {
		Biblioteca biblioteca = new Biblioteca();
		Livro livro = new Livro("Livro 1");
		Usuario usuario = new Usuario ("Usuario 1");
		biblioteca.emprestaLivro(usuario, livro);
		biblioteca.devolveLivro(livro);
		assertFalse(livro.estaEmprestado());
		assertTrue(livro.estaDisponivel());
	}
	
	@Test
	public void testaListaDisponiveisEIndisponiveis () {
		Biblioteca biblioteca = new Biblioteca();
		Livro[] livros = new Livro[6];
		for (int i = 0; i < 6; i++) {
			livros[i] = new Livro("Livro " + (i + 1));
			biblioteca.adicionaLivroAoCatalogo(livros[i]);
		}
		Usuario[] usuarios = new Usuario[2];
		for (int i = 0; i < 2; i++) {
			usuarios[i] = new Usuario("Usuario " + (i + 1));
			biblioteca.adicionaLivroAoCatalogo(livros[i]);
			biblioteca.registraUsuario(usuarios[i]);
		}
		biblioteca.emprestaLivro(usuarios[0], livros[0]);
		biblioteca.emprestaLivro(usuarios[0], livros[2]);
		biblioteca.emprestaLivro(usuarios[1], livros[1]);
		biblioteca.emprestaLivro(usuarios[1], livros[5]);
		biblioteca.exibeLivrosDisponiveisEmprestimo();
		biblioteca.exibeLivrosIndisponiveisEmprestimo();
	}
}
