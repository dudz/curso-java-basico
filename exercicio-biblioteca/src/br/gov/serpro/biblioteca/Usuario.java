package br.gov.serpro.biblioteca;

import java.util.Set;
import java.util.HashSet;

public class Usuario {
	private String nome;
	private Set<Livro> livrosEmprestados = new HashSet<Livro>();
	
	
	public Usuario (String nome) {
		this.nome = nome;
	}
	
	public void anexaLivroAListaDeEmprestados (Livro livro) {
		livrosEmprestados.add(livro);
	}
	
	public void desanexaLivroDaListaDeEmprestados (Livro livro) {
		livrosEmprestados.remove(livro);
	}
	
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString () {
		return nome;
	}
	
	@Override
	public boolean equals (Object o) {
		if (this == o) return true;
		if (o == null || (o instanceof Usuario) == false) return false;
		if (nome == ((Usuario)o).nome) return true;
		return false;
	}
	
	@Override
	public int hashCode() {
		return nome.hashCode();
	}

}
