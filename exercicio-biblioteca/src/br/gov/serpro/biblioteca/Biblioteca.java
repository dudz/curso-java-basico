package br.gov.serpro.biblioteca;

import java.util.Set;
import java.util.HashSet;

public class Biblioteca {

	private Set<Usuario> listaUsuarios = new HashSet<Usuario>();
	private Set<Livro> catalogoLivros = new HashSet<Livro>();
	
	public void registraUsuario (Usuario usuario) {
		listaUsuarios.add(usuario);
	}
	
	public boolean existeUsuario (Usuario usuario) {
		return listaUsuarios.contains(usuario);
	}
	
	public void adicionaLivroAoCatalogo (Livro livro) {
		catalogoLivros.add(livro);
	}
	
	public boolean existeLivroNoCatalogo (Livro livro) {
		return catalogoLivros.contains(livro);
	}
	
	public void emprestaLivro (Usuario usuario, Livro livro) {
		livro.anexaUsuarioDoEmprestimo(usuario);
		usuario.anexaLivroAListaDeEmprestados(livro);
	}
	
	public void devolveLivro (Livro livro) {
		if (livro.estaEmprestado()) {
			livro.usuarioDoEmprestimo().desanexaLivroDaListaDeEmprestados(livro);
		}
		livro.desanexaUsuarioDoEmprestimo();
	}
	
	public void exibeLivrosDisponiveisEmprestimo() {
		System.out.println("Livros disponiveis");
		for (Livro l : catalogoLivros) {
			if (l.estaDisponivel()) {
				System.out.println(l);
			}
		}
	}

	public void exibeLivrosIndisponiveisEmprestimo() {
		System.out.println("Livros indisponiveis");
		for (Livro l : catalogoLivros) {
			if (l.estaDisponivel() == false) {
				System.out.println(l);
			}
		}
	}
}
