package br.gov.serpro.biblioteca;

public class Livro {
	private String nome;
	private Usuario usuarioEmprestimo = null;
	
	public Livro (String nome) {
		this.nome = nome;
	}
	
	public void anexaUsuarioDoEmprestimo (Usuario usuario) {
		if (usuarioEmprestimo != null) {
			throw new RuntimeException("Livro ja emprestado para " + usuarioEmprestimo);
		}
		usuarioEmprestimo = usuario;		
	}
	
	public Usuario usuarioDoEmprestimo () {
		return usuarioEmprestimo;
	}
	
	public boolean estaDisponivel () {
		return usuarioEmprestimo == null;
	}

	public boolean estaEmprestado () {
		return usuarioEmprestimo != null;
	}
	
	public void desanexaUsuarioDoEmprestimo () {
		usuarioEmprestimo = null;		
	}
	
	public String getNome () {
		return nome;
	}
	
	@Override
	public String toString () {
		return nome;
	}
	
	@Override
	public boolean equals (Object o) {
		if (this == o) return true;
		if (o == null || (o instanceof Livro) == false) return false;
		if (nome == ((Livro)o).nome) return true;
		return false;
	}
	
	@Override
	public int hashCode() {
		return nome.hashCode();
	}
}
