package br.gov.serpro.banco;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.Test;

public class TestadorOperacao {

	@Test
	public void deveRecuperarAtributosCorretamente() {
		Operacao operacao = new Operacao(new BigDecimal("1000.00"), "Saque", new BigDecimal("100.00"));
		assertEquals(operacao.getOperacao(), "Saque");
		assertEquals(operacao.getSaldoAnterior(), new BigDecimal("1000.00"));
		assertEquals(operacao.getValor(), new BigDecimal("100.00"));
		LocalDateTime dataOperacao = operacao.getDataOperacao();
		// Como foi usado relógio real, testar faixa de tempo assumindo um segundo.
		assertTrue(dataOperacao.compareTo(LocalDateTime.now()) <= 0);
		assertTrue(dataOperacao.compareTo(LocalDateTime.now().minusSeconds(1)) > 0);
	}

	@Test
	public void deveAdicionarContaTransferência() {
		Operacao operacao = new Operacao(new BigDecimal("1000.00"), "Transferência", new BigDecimal("100.00"));
		operacao.adicionaNrContaOperacaoTransferencia("31415");
		assertEquals(operacao.getOperacao(), "Transferência conta 31415");
	}
	
}
