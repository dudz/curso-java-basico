package br.gov.serpro.bancoalpha;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.Test;

import br.gov.serpro.banco.SaldoInsuficienteException;


public class TestadorContaEspecial {

	@Test
	public void deveConstruirContaComSaldo() {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		assertEquals(contaEspecial.obterSaldo(), new BigDecimal("1000.00"));
	}

	@Test
	public void deveConstruirContaSaldoZerado() {
		ContaEspecial contaEspecial = new ContaEspecial("1");
		assertEquals(contaEspecial.obterSaldo(), BigDecimal.ZERO);
	}

	@Test
	public void deveNaoTerOperacaoDeposito() {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		assertThrows(Exception.class, () ->  
			contaEspecial.depositar(new BigDecimal("123.00"))
		);
	}
	
	@Test
	public void deveSacarCorretamente() throws Exception {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		contaEspecial.saque(new BigDecimal("100.00"));
		assertEquals(contaEspecial.obterSaldo(), new BigDecimal("900.00"));
	}

	@Test
	public void deveSacarUsandoLimite() throws Exception {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		contaEspecial.saque(new BigDecimal("2000.00"));
		assertEquals(contaEspecial.obterSaldo(), new BigDecimal("-1000.00"));
	}
	
	@Test
	public void deveExcederLimiteNoSaque() {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		assertThrows(SaldoInsuficienteException.class, () ->
			contaEspecial.saque(new BigDecimal("2000.01"))
		);
	}
	
	@Test
	public void deveReceberTransferenciaCorretamente() {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		contaEspecial.receberTransferencia("123", new BigDecimal("123.00"));
		assertEquals(contaEspecial.obterSaldo(), new BigDecimal("1123.00"));
	}

	@Test
	public void deveEnviarTransferenciaCorretamente() throws Exception {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		contaEspecial.enviarTransferencia("123", new BigDecimal("123.00"));
		assertEquals(contaEspecial.obterSaldo(), new BigDecimal("877.00"));
	}
	
	@Test
	public void deveExcederLimiteNaTransferencia() {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		assertThrows(SaldoInsuficienteException.class, () ->
			contaEspecial.enviarTransferencia("123", new BigDecimal("2000.01"))
		);
	}
	
	@Test
	public void deveObterListaExtrato() throws Exception {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		contaEspecial.saque(new BigDecimal("1.00"));
		assertEquals(contaEspecial.extrato().size(), 1);
	}
	
	@Test
	public void deveExistirMetodoValidacaoDummy() {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		contaEspecial.validarQuantidadeOperacoes();
	}
	
	@Test
	public void numeroContaDeveIdentificarUnivocamenteObjeto() {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		ContaEspecial outraContaEspecial = new ContaEspecial("1", new BigDecimal("3100.00"));
		assertEquals(contaEspecial, outraContaEspecial);
		assertEquals(contaEspecial.hashCode(), outraContaEspecial.hashCode());
		outraContaEspecial = new ContaEspecial("2", new BigDecimal("1000.00"));
		assertNotEquals(contaEspecial, outraContaEspecial);
		assertNotEquals(contaEspecial.hashCode(), outraContaEspecial.hashCode());
	}
	
	@Test
	public void deveImplementarComparable() {
		ContaEspecial contaEspecial = new ContaEspecial("1", new BigDecimal("1000.00"));
		assertEquals(contaEspecial.compareTo(contaEspecial), 0);
		assertNotEquals(contaEspecial.compareTo(null), 0);
		assertNotEquals(contaEspecial.compareTo(new String("a")), 0);
	}
}
