package br.gov.serpro.bancoalpha;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import br.gov.serpro.banco.Conta;
import br.gov.serpro.banco.Operacao;
import br.gov.serpro.banco.OperacaoNaoValidadaException;
import br.gov.serpro.banco.Usuario;

public class TestadorBancoAlpha {

	@Test
	public void deveTerApenasUmaInstancia() {
		BancoAlpha banco;
		BancoAlpha outroBancoAlpha;

		banco = BancoAlpha.getInstance();
		outroBancoAlpha = BancoAlpha.getInstance();

		assertSame(banco, outroBancoAlpha);
	}

	@Test
	public void deveAdicionarConta() {
		int numeroConta;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();

		do {
			numeroConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroConta)));
		Conta conta = new ContaComum(String.valueOf(numeroConta));
		banco.adicionarConta(conta);
		assertTrue(banco.contaExistente(String.valueOf(numeroConta)));

		do {
			numeroConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroConta)));
		conta = new ContaEspecial(String.valueOf(numeroConta));
		banco.adicionarConta(conta);
		assertTrue(banco.contaExistente(String.valueOf(numeroConta)));
	}

	@Test
	public void naoDeveAdicionarContaDuplicada() {
		int numeroContaInexistente;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();

		// achar uma conta ainda inexistente
		do {
			numeroContaInexistente = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroContaInexistente)));
		Conta conta = new ContaComum(String.valueOf(numeroContaInexistente));
		banco.adicionarConta(conta);
		final int numeroConta = numeroContaInexistente;
		assertThrows(RuntimeException.class,
				() -> banco.adicionarConta(new ContaEspecial(String.valueOf(numeroConta))));
	}

	@Test
	public void deveRecuperarContaAPartirDoNumero() {
		Conta conta;
		Conta outraConta;
		String numeroConta = "1";
		BancoAlpha banco = BancoAlpha.getInstance();
		if (banco.contaExistente(numeroConta)) {
			conta = banco.obterContaComNumero(numeroConta);
		} else {
			conta = new ContaComum(numeroConta);
			banco.adicionarConta(conta);
		}
		outraConta = banco.obterContaComNumero(numeroConta);
		assertSame(conta, outraConta);
	}

	@Test
	public void naoDeveTratarContaInexistente() {
		int numeroContaInexistente;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();
		Conta contaInexistente;
		// achar uma conta ainda inexistente
		do {
			numeroContaInexistente = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroContaInexistente)));
		final int numeroContaInexistenteFinal = numeroContaInexistente;
		contaInexistente = new ContaComum(String.valueOf(numeroContaInexistenteFinal));
		Usuario usuarioContaInexistente = new UsuarioBanco("Usuario Inexistente", contaInexistente);
		// assertThrows(IllegalArgumentException.class, () ->
		// banco.consultarExtrato(null));
		assertThrows(RuntimeException.class, () -> banco.consultarExtrato(usuarioContaInexistente));
		Conta outraConta = new ContaComum("1");
		if (banco.contaExistente(outraConta.obterNrConta())) {
			outraConta = banco.obterContaComNumero(outraConta.obterNrConta());
		} else {
			banco.adicionarConta(outraConta);
		}
		Conta contaExistente = outraConta;
		assertThrows(RuntimeException.class, () -> banco.realizarTransferencia(usuarioContaInexistente,
				contaExistente.obterNrConta(), new BigDecimal("1.00")));
		int numeroOutraContaInexistente;
		do {
			numeroOutraContaInexistente = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroOutraContaInexistente)));
		Usuario usuarioContaExistente = new UsuarioBanco("Usuario Existente", contaExistente);
		assertThrows(RuntimeException.class, () -> banco.realizarTransferencia(usuarioContaExistente,
				contaInexistente.obterNrConta(), new BigDecimal("1.00")));
		assertThrows(RuntimeException.class, () -> banco.saque(usuarioContaInexistente, new BigDecimal("1.00")));
		assertThrows(RuntimeException.class, () -> banco.validarOperacao(usuarioContaInexistente));
		assertThrows(RuntimeException.class, () -> banco.consultarSaldo(usuarioContaInexistente));
	}

	@Test
	public void naoDeveTratarUsuarioContaInvalidos() {
		BancoAlpha banco = BancoAlpha.getInstance();
		assertThrows(IllegalArgumentException.class, () -> banco.consultarExtrato(null));
		Conta contaExistente = new ContaComum("1");
		if (banco.contaExistente(contaExistente.obterNrConta())) {
			contaExistente = banco.obterContaComNumero(contaExistente.obterNrConta());
		} else {
			banco.adicionarConta(contaExistente);
		}
		Conta contaExistenteFinal = contaExistente;
		assertThrows(IllegalArgumentException.class,
				() -> banco.realizarTransferencia(null, contaExistenteFinal.obterNrConta(), new BigDecimal("1.00")));
		Usuario usuarioContaExistente = new UsuarioBanco("Usuario Existente", contaExistente);
		assertThrows(IllegalArgumentException.class,
				() -> banco.realizarTransferencia(usuarioContaExistente, null, new BigDecimal("1.00")));
		assertThrows(IllegalArgumentException.class, () -> banco.saque(null, new BigDecimal("1.00")));
		assertThrows(IllegalArgumentException.class, () -> banco.validarOperacao(null));
		assertThrows(IllegalArgumentException.class, () -> banco.consultarSaldo(null));

	}

	@Test
	public void deveObterExtrato() {
		Conta conta;
		String numeroConta = "1";
		BancoAlpha banco = BancoAlpha.getInstance();
		if (banco.contaExistente(numeroConta)) {
			conta = banco.obterContaComNumero(numeroConta);
		} else {
			conta = new ContaComum(numeroConta);
			banco.adicionarConta(conta);
		}
		Usuario usuario = new UsuarioBanco("Usuario", conta);
		List<Operacao> extrato = banco.consultarExtrato(usuario);
		assertNotNull(extrato);
	}

	@Test
	public void deveConsultarSaldoCorreto() {
		int numeroConta;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();

		do {
			numeroConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroConta)));
		Conta conta = new ContaComum(String.valueOf(numeroConta), new BigDecimal("3141.59"));
		banco.adicionarConta(conta);
		Usuario usuario = new UsuarioBanco("Usuario", conta);
		assertEquals(banco.consultarSaldo(usuario), new BigDecimal("3141.59"));
	}

	@Test
	public void naoDeveRealizarDeposito() {
		int numeroConta;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();

		do {
			numeroConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroConta)));
		Conta conta = new ContaComum(String.valueOf(numeroConta));
		banco.adicionarConta(conta);
		Usuario usuario = new UsuarioBanco("Usuario", conta);
		assertThrows(RuntimeException.class, () -> banco.realizarDeposito(usuario, new BigDecimal("3141.59")));
	}

	@Test
	public void deveRealizarSaqueCorretamente() throws Exception {
		int numeroConta;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();

		do {
			numeroConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroConta)));
		Conta conta = new ContaComum(String.valueOf(numeroConta), new BigDecimal("1000.00"));
		banco.adicionarConta(conta);
		Usuario usuario = new UsuarioBanco("Usuario", conta);
		banco.saque(usuario, new BigDecimal("100.00"));
		assertEquals(banco.consultarSaldo(usuario), new BigDecimal("900.00"));
	}
	
	@Test
	public void naoDeveSacarValorInvalido() throws Exception {
		int numeroConta;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();

		do {
			numeroConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroConta)));
		Conta conta = new ContaComum(String.valueOf(numeroConta), new BigDecimal("1000.00"));
		banco.adicionarConta(conta);
		Usuario usuario = new UsuarioBanco("Usuario", conta);
		assertThrows(IllegalArgumentException.class, () -> banco.saque(usuario, new BigDecimal("0.00")));
		assertThrows(IllegalArgumentException.class, () -> banco.saque(usuario, new BigDecimal("-1.00")));
		assertThrows(IllegalArgumentException.class, () -> banco.saque(usuario, null));
	}
	
	@Test
	public void deveRealizarTransferenciaCorretamente() throws Exception {
		int numeroConta;
		int numeroOutraConta;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();

		do {
			numeroConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroConta)));
		Conta conta = new ContaComum(String.valueOf(numeroConta), new BigDecimal("1000.00"));
		banco.adicionarConta(conta);

		do {
			numeroOutraConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroOutraConta)));
		Conta outraConta = new ContaComum(String.valueOf(numeroOutraConta));
		banco.adicionarConta(outraConta);
		
		Usuario usuario = new UsuarioBanco("Usuario", conta);
		banco.realizarTransferencia(usuario, outraConta.obterNrConta(), new BigDecimal("100.00"));
		assertEquals(conta.obterSaldo(), new BigDecimal("900.00"));
		assertEquals(outraConta.obterSaldo(), new BigDecimal("100.00"));
	}
	
	@Test
	public void naoDeveTransferirValorInvalido() throws Exception {
		int numeroConta;
		int numeroOutraConta;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();

		do {
			numeroConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroConta)));
		Conta conta = new ContaComum(String.valueOf(numeroConta), new BigDecimal("1000.00"));
		banco.adicionarConta(conta);

		do {
			numeroOutraConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroOutraConta)));
		Conta outraConta = new ContaComum(String.valueOf(numeroOutraConta));
		banco.adicionarConta(outraConta);

		Usuario usuario = new UsuarioBanco("Usuario", conta);
		assertThrows(IllegalArgumentException.class,
				() -> banco.realizarTransferencia(usuario, outraConta.obterNrConta(), new BigDecimal("0.00")));
		assertThrows(IllegalArgumentException.class,
				() -> banco.realizarTransferencia(usuario, outraConta.obterNrConta(), new BigDecimal("-1.00")));
		assertThrows(IllegalArgumentException.class,
				() -> banco.realizarTransferencia(usuario, outraConta.obterNrConta(), null));
	}

	@Test
	public void naoDeveExcederQuantidadeOperacoes() throws Exception {
		int numeroConta;
		int numeroOutraConta;
		BancoAlpha banco = BancoAlpha.getInstance();
		Random random = new Random();

		do {
			numeroConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroConta)));
		Conta conta = new ContaComum(String.valueOf(numeroConta), new BigDecimal("1000.00"));
		banco.adicionarConta(conta);

		do {
			numeroOutraConta = random.nextInt(1000000 - 1) + 1;
		} while (banco.contaExistente(String.valueOf(numeroOutraConta)));
		Conta outraConta = new ContaComum(String.valueOf(numeroOutraConta));
		banco.adicionarConta(outraConta);

		Usuario usuario = new UsuarioBanco("Usuario", conta);

		banco.saque(usuario, new BigDecimal("10.00"));
		banco.saque(usuario, new BigDecimal("10.00"));
		banco.saque(usuario, new BigDecimal("10.00"));
		
		assertThrows(RuntimeException.class,
				() -> banco.saque(usuario, new BigDecimal("10.00")));
		assertThrows(RuntimeException.class,
				() -> banco.realizarTransferencia(usuario, outraConta.obterNrConta(), new BigDecimal("10.00")));
		assertThrows(OperacaoNaoValidadaException.class,
				() -> banco.validarOperacao(usuario));
	}
}
