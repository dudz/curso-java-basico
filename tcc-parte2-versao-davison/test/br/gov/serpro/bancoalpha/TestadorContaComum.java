package br.gov.serpro.bancoalpha;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.Test;

import br.gov.serpro.banco.SaldoInsuficienteException;


public class TestadorContaComum {

	@Test
	public void deveConstruirContaComSaldo() {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		assertEquals(contaComum.obterSaldo(), new BigDecimal("1000.00"));
	}

	@Test
	public void deveConstruirContaSaldoZerado() {
		ContaComum contaComum = new ContaComum("1");
		assertEquals(contaComum.obterSaldo(), BigDecimal.ZERO);
	}

	@Test
	public void deveNaoTerOperacaoDeposito() {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		assertThrows(Exception.class, () ->  
			contaComum.depositar(new BigDecimal("123.00"))
		);
	}
	
	@Test
	public void deveSacarCorretamente() throws Exception {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		contaComum.saque(new BigDecimal("100.00"));
		assertEquals(contaComum.obterSaldo(), new BigDecimal("900.00"));
	}

	@Test
	public void deveExcederLimiteNoSaque() {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		assertThrows(SaldoInsuficienteException.class, () ->
			contaComum.saque(new BigDecimal("1000.01"))
		);
	}
	
	@Test
	public void deveReceberTransferenciaCorretamente() {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		contaComum.receberTransferencia("123", new BigDecimal("123.00"));
		assertEquals(contaComum.obterSaldo(), new BigDecimal("1123.00"));
	}

	@Test
	public void deveEnviarTransferenciaCorretamente() throws Exception {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		contaComum.enviarTransferencia("123", new BigDecimal("123.00"));
		assertEquals(contaComum.obterSaldo(), new BigDecimal("877.00"));
	}
	
	@Test
	public void deveExcederLimiteNaTransferencia() {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		assertThrows(SaldoInsuficienteException.class, () ->
			contaComum.enviarTransferencia("123", new BigDecimal("2000.01"))
		);
	}
	
	@Test
	public void deveObterListaExtrato() throws Exception {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		contaComum.saque(new BigDecimal("1.00"));
		assertEquals(contaComum.extrato().size(), 1);
	}
	
	@Test
	public void deveExcederQuantidadeOperacoes() throws SaldoInsuficienteException {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		contaComum.saque(new BigDecimal("1.00"));
		contaComum.saque(new BigDecimal("1.00"));
		contaComum.saque(new BigDecimal("1.00"));
		assertThrows(RuntimeException.class, () ->
			contaComum.saque(new BigDecimal("1.00"))
		);
		assertThrows(RuntimeException.class, () ->
			contaComum.enviarTransferencia("123", new BigDecimal("100.00"))
		);
	}
	
	@Test
	public void numeroContaDeveIdentificarUnivocamenteObjeto() {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		ContaComum outraContaEspecial = new ContaComum("1", new BigDecimal("3100.00"));
		assertEquals(contaComum, outraContaEspecial);
		assertEquals(contaComum.hashCode(), outraContaEspecial.hashCode());
		outraContaEspecial = new ContaComum("2", new BigDecimal("1000.00"));
		assertNotEquals(contaComum, outraContaEspecial);
		assertNotEquals(contaComum.hashCode(), outraContaEspecial.hashCode());
	}
	
	@Test
	public void deveImplementarComparable() {
		ContaComum contaComum = new ContaComum("1", new BigDecimal("1000.00"));
		assertEquals(contaComum.compareTo(contaComum), 0);
		assertNotEquals(contaComum.compareTo(null), 0);
		assertNotEquals(contaComum.compareTo(new String("a")), 0);
	}
}
