package br.gov.serpro.bancobeta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.Test;

import br.gov.serpro.banco.SaldoInsuficienteException;


public class TestadorContaPremium {

	@Test
	public void deveConstruirContaComSaldo() {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		assertEquals(contaPremium.obterSaldo(), new BigDecimal("1000.00"));
	}

	@Test
	public void deveConstruirContaSaldoZerado() {
		ContaPremium contaPremium = new ContaPremium("1");
		assertEquals(contaPremium.obterSaldo(), BigDecimal.ZERO);
	}

	@Test
	public void deveDepositarCorretamente() {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		contaPremium.depositar(new BigDecimal("123.00"));
		assertEquals(contaPremium.obterSaldo(), new BigDecimal("1123.00"));
	}
	
	@Test
	public void deveSacarCorretamente() throws Exception {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		contaPremium.saque(new BigDecimal("100.00"));
		assertEquals(contaPremium.obterSaldo(), new BigDecimal("898.50"));
	}

	@Test
	public void deveSacarUsandoLimite() throws Exception {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		contaPremium.saque(new BigDecimal("2000.00"));
		assertEquals(contaPremium.obterSaldo(), new BigDecimal("-1011.00"));
	}
	
	@Test
	public void deveExcederLimiteNoSaque() {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		assertThrows(SaldoInsuficienteException.class, () ->
			contaPremium.saque(new BigDecimal("6000.01"))
		);
	}
	
	@Test
	public void deveReceberTransferenciaCorretamente() {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		contaPremium.receberTransferencia("123", new BigDecimal("123.00"));
		assertEquals(contaPremium.obterSaldo(), new BigDecimal("1123.00"));
	}

	@Test
	public void deveEnviarTransferenciaCorretamente() throws Exception {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		contaPremium.enviarTransferencia("123", new BigDecimal("123.00"));
		assertEquals(contaPremium.obterSaldo(), new BigDecimal("876.00"));
	}
	
	@Test
	public void deveExcederLimiteNaTransferencia() {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		assertThrows(SaldoInsuficienteException.class, () ->
			contaPremium.enviarTransferencia("123", new BigDecimal("6000.01"))
		);
	}
	
	@Test
	public void deveObterListaExtrato() throws Exception {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		contaPremium.saque(new BigDecimal("1.00"));
		assertEquals(contaPremium.extrato().size(), 1);
	}

	@Test
	public void deveDarSaldoInsuficienteExtrato() {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("-4999.99"));
		assertThrows(RuntimeException.class, () -> contaPremium.extrato());
	}

	@Test
	public void deveExistirMetodoValidacaoDummy() throws Exception {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		contaPremium.validarQuantidadeOperacoes();
	}
	
	@Test
	public void numeroContaDeveIdentificarUnivocamenteObjeto() {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		ContaPremium outraContaEspecial = new ContaPremium("1", new BigDecimal("3100.00"));
		assertEquals(contaPremium, outraContaEspecial);
		assertEquals(contaPremium.hashCode(), outraContaEspecial.hashCode());
		outraContaEspecial = new ContaPremium("2", new BigDecimal("1000.00"));
		assertNotEquals(contaPremium, outraContaEspecial);
		assertNotEquals(contaPremium.hashCode(), outraContaEspecial.hashCode());
	}
	
	@Test
	public void deveImplementarComparable() {
		ContaPremium contaPremium = new ContaPremium("1", new BigDecimal("1000.00"));
		assertEquals(contaPremium.compareTo(contaPremium), 0);
		assertNotEquals(contaPremium.compareTo(null), 0);
		assertNotEquals(contaPremium.compareTo(new String("a")), 0);
	}
}
