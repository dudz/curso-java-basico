package br.gov.serpro.bancoalpha;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import br.gov.serpro.banco.Conta;
import br.gov.serpro.banco.Operacao;
import br.gov.serpro.banco.OperacaoNaoValidadaException;
import br.gov.serpro.banco.SaldoInsuficienteException;

public final class ContaComum extends Conta implements Comparable<Object> {

	private static final int MAXIMO_OPERACOES_DIARIAS = 3;
	private LocalDate dataUltimaOperacao = LocalDate.of(1, 1, 1);
	private Integer quantidadeOperacoesNoDia = 0;
	private Integer hashCode = null;
	
	private List<Operacao> operacoesRealizadas = new LinkedList<Operacao>();
	
	public ContaComum(String numeroConta) {
		super(numeroConta);
	}
	
	public ContaComum(String numeroConta, BigDecimal saldo) {
		super(numeroConta, saldo);
	}

	@Override
	public void depositar(BigDecimal valor) {
		throw new RuntimeException("Operação de depósito não disponível.");
	}

	@Override
	public void saque(BigDecimal valor) throws SaldoInsuficienteException {
		try {
			validarQuantidadeOperacoes();
		}
		catch (OperacaoNaoValidadaException e) {
			throw new RuntimeException(e.getMessage());
		}
		if (valor.compareTo(this.obterSaldo()) > 0) {
			throw new SaldoInsuficienteException("Saldo insuficiente para sacar " + valor.toString());
		}
		Operacao operacao = new Operacao(this.obterSaldo(), "Saque", valor);
		this.operacoesRealizadas.add(operacao);
		quantidadeOperacoesNoDia++;
		this.diminuirSaldo(valor);
	}

	@Override
	public void receberTransferencia(String nrContaOrigem, BigDecimal valor) {
		Operacao operacao = new Operacao(this.obterSaldo(), "Recebimento Transferência", valor);
		operacao.adicionaNrContaOperacaoTransferencia(nrContaOrigem);
		this.operacoesRealizadas.add(operacao);
		this.aumentarSaldo(valor);
	}

	@Override
	public void enviarTransferencia(String nrContaDestino, BigDecimal valor) throws SaldoInsuficienteException {
		try {
			validarQuantidadeOperacoes();
		}
		catch (OperacaoNaoValidadaException e) {
			throw new RuntimeException(e.getMessage());
		}
		if (valor.compareTo(this.obterSaldo()) > 0) {
			throw new SaldoInsuficienteException("Saldo insuficiente para transferir " + valor.toString());
		}
		
		Operacao operacao = new Operacao(this.obterSaldo(), "Transferência", valor);
		operacao.adicionaNrContaOperacaoTransferencia(nrContaDestino);
		this.operacoesRealizadas.add(operacao);
		quantidadeOperacoesNoDia++;
		this.diminuirSaldo(valor);
	}

	@Override
	public List<Operacao> extrato() {
		return this.operacoesRealizadas;
	}
	
	@Override
	public void validarQuantidadeOperacoes () throws OperacaoNaoValidadaException{
		if (dataUltimaOperacao.equals(LocalDate.now()) == false) {
			synchronized (dataUltimaOperacao) {
				dataUltimaOperacao = LocalDate.now();
				quantidadeOperacoesNoDia = 0;
			}
		}
		if (quantidadeOperacoesNoDia >= MAXIMO_OPERACOES_DIARIAS) {
			throw new OperacaoNaoValidadaException("Excedido o número máximo de " + MAXIMO_OPERACOES_DIARIAS
					+ " operações diárias.");
		}
	}

	@Override
	public boolean equals(Object o) {
		return compareTo(o) == 0;
	}

	@Override
	public int hashCode() {
		if (hashCode == null) {
			hashCode = getClass().getSuperclass().getName().concat(obterNrConta()).hashCode();
		}
		return hashCode.intValue();
	}

	@Override
	public int compareTo(Object o) {
		if (this == o) return 0;
		if (o == null) return 1;
		if ((o instanceof Conta) == false) {
			return getClass().getSuperclass().getName().compareTo(o.getClass().getName());
		}
		return obterNrConta().compareTo(((Conta)o).obterNrConta());
	}
}
