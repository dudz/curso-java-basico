package br.gov.serpro.bancoalpha;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import br.gov.serpro.banco.Conta;
import br.gov.serpro.banco.Operacao;
import br.gov.serpro.banco.SaldoInsuficienteException;

public class ContaEspecial extends Conta implements Comparable<Object> {

	private static final BigDecimal LIMITE_CONTA_ESPECIAL = new BigDecimal("1000.00");
	private Integer quantidadeOperacoesNoDia = 0;
	private Integer hashCode = null;
	
	private List<Operacao> operacoesRealizadas = new LinkedList<Operacao>();
	
	public ContaEspecial(String numeroConta) {
		super(numeroConta);
	}
	
	public ContaEspecial(String numeroConta, BigDecimal saldo) {
		super(numeroConta, saldo);
	}

	@Override
	public void depositar(BigDecimal valor) {
		throw new RuntimeException("Operação de depósito não disponível.");
	}

	@Override
	public void saque(BigDecimal valor) throws SaldoInsuficienteException {
		if (valor.compareTo(this.obterSaldo().add(LIMITE_CONTA_ESPECIAL)) > 0) {
			throw new SaldoInsuficienteException("Saldo insuficiente para sacar " + valor.toString());
		}
		Operacao operacao = new Operacao(this.obterSaldo(), "Saque", valor);
		this.operacoesRealizadas.add(operacao);
		quantidadeOperacoesNoDia++;
		this.diminuirSaldo(valor);
	}

	@Override
	public void receberTransferencia(String nrContaOrigem, BigDecimal valor) {
		Operacao operacao = new Operacao(this.obterSaldo(), "Recebimento Transferência", valor);
		operacao.adicionaNrContaOperacaoTransferencia(nrContaOrigem);
		this.operacoesRealizadas.add(operacao);
		this.aumentarSaldo(valor);
	}

	@Override
	public void enviarTransferencia(String nrContaDestino, BigDecimal valor) throws SaldoInsuficienteException {
		if (valor.compareTo(this.obterSaldo().add(LIMITE_CONTA_ESPECIAL)) > 0) {
			throw new SaldoInsuficienteException("Saldo insuficiente para transferir " + valor.toString());
		}
		Operacao operacao = new Operacao(this.obterSaldo(), "Transferência", valor);
		operacao.adicionaNrContaOperacaoTransferencia(nrContaDestino);
		this.operacoesRealizadas.add(operacao);
		quantidadeOperacoesNoDia++;
		this.diminuirSaldo(valor);
	}

	@Override
	public List<Operacao> extrato() {
		return this.operacoesRealizadas;
	}
	
	@Override
	public void validarQuantidadeOperacoes () {
	}

	@Override
	public boolean equals(Object o) {
		return compareTo(o) == 0;
	}

	@Override
	public int hashCode() {
		if (hashCode == null) {
			hashCode = getClass().getSuperclass().getName().concat(obterNrConta()).hashCode();
		}
		return hashCode.intValue();
	}

	@Override
	public int compareTo(Object o) {
		if (this == o) return 0;
		if (o == null) return 1;
		if ((o instanceof Conta) == false) {
			return getClass().getSuperclass().getName().compareTo(o.getClass().getName());
		}
		return obterNrConta().compareTo(((Conta)o).obterNrConta());
	}
}
