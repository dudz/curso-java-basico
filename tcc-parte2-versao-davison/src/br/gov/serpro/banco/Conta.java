package br.gov.serpro.banco;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public abstract class Conta {
	
	private BigDecimal saldo;
	private String nrConta;

	public String obterNrConta() {
		return nrConta;
	}
	
	public BigDecimal obterSaldo() {
		return saldo;
	}
	
	public abstract void depositar(BigDecimal valor) ;
	
	public abstract void saque(BigDecimal valor) throws SaldoInsuficienteException;
	
	public abstract void receberTransferencia(String nrContaOrigem, BigDecimal valor);
	
	public abstract void enviarTransferencia(String nrContaDestino, BigDecimal valor) throws SaldoInsuficienteException;
	
	public abstract List<Operacao> extrato();

	
	/*
	 * Refactoring por Eduardo: Não era possível mexer no saldo nem contruir conta com número.
	 */
	
	protected void aumentarSaldo(BigDecimal valor) {
		saldo = saldo.add(valor).setScale(2, RoundingMode.HALF_EVEN);
	}

	protected void diminuirSaldo(BigDecimal valor) {
		saldo = saldo.subtract(valor).setScale(2, RoundingMode.HALF_EVEN);
	}
	
	public Conta(String numeroConta, BigDecimal saldoInicial) {
		nrConta = numeroConta;
		saldo = saldoInicial;
	}

	public Conta(String numeroConta) {
		nrConta = numeroConta;
		saldo = BigDecimal.ZERO;
	}

	// A validação de operação exigida pela Interface BancoOperavel é de responsabilidade
	// da conta, por isso precisa ter visibilidade dessa validação.
	public abstract void validarQuantidadeOperacoes () throws OperacaoNaoValidadaException;
}
