package br.gov.serpro.banco;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Operacao {
	
	private BigDecimal saldoAnterior;
	private String operacao;
	private BigDecimal valor;
	private LocalDateTime dataOperacao;
	
	public Operacao(BigDecimal saldoAnterior, String operacao, BigDecimal valor) {
		this.saldoAnterior = saldoAnterior;
		this.operacao = operacao;
		this.valor = valor;
		this.dataOperacao = LocalDateTime.now();
	}
	
	public void adicionaNrContaOperacaoTransferencia (String nrContaTransferenciaEfetivada) {
		this.operacao = operacao.concat(" conta " + nrContaTransferenciaEfetivada);
	}

	// Métodos adicionados por José Eduardo. Motivo: Atributos privados inacessíveis originalmente.

	public BigDecimal getSaldoAnterior() {
		return saldoAnterior;
	}

	public String getOperacao() {
		return operacao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public LocalDateTime getDataOperacao() {
		return dataOperacao;
	}
}
