package br.gov.serpro.bancobeta;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

import br.gov.serpro.banco.Conta;
import br.gov.serpro.banco.Operacao;
import br.gov.serpro.banco.OperacaoNaoValidadaException;
import br.gov.serpro.banco.SaldoInsuficienteException;

public final class ContaPremium extends Conta implements Comparable<Object> {

	private static final BigDecimal LIMITE_CONTA_PREMIUM = new BigDecimal("5000.00");
	private static final BigDecimal TAXA_EXTRATO = new BigDecimal("0.50");
	private static final BigDecimal TAXA_TRANSFERENCIA = new BigDecimal("1.00");
	private static final BigDecimal TAXA_SAQUE = new BigDecimal("1.00");
	private static final BigDecimal TAXA_PERCENTUAL_SAQUE = new BigDecimal("0.005");
	private Integer quantidadeOperacoesNoDia = 0;
	private Integer hashCode = null;
	
	private List<Operacao> operacoesRealizadas = new LinkedList<Operacao>();
	
	public ContaPremium(String numeroConta) {
		super(numeroConta);
	}

	public ContaPremium(String numeroConta, BigDecimal saldo) {
		super(numeroConta, saldo);
	}
	
	@Override
	public void depositar(BigDecimal valor) {
		Operacao operacao = new Operacao(this.obterSaldo(), "Depósito", valor);
		this.operacoesRealizadas.add(operacao);
		quantidadeOperacoesNoDia++;
		this.aumentarSaldo(valor);
	}

	@Override
	public void saque(BigDecimal valor) throws SaldoInsuficienteException {
		BigDecimal valorComTaxas;
		valorComTaxas = TAXA_SAQUE
				.add(valor.multiply(TAXA_PERCENTUAL_SAQUE, MathContext.UNLIMITED).setScale(2, RoundingMode.FLOOR))
				.add(valor);
		if (valorComTaxas.compareTo(this.obterSaldo().add(LIMITE_CONTA_PREMIUM)) > 0) {
			throw new SaldoInsuficienteException("Saldo insuficiente para sacar " + valor.toString());
		}
		Operacao operacao = new Operacao(this.obterSaldo(), "Saque", valor);
		this.operacoesRealizadas.add(operacao);
		quantidadeOperacoesNoDia++;
		this.diminuirSaldo(valorComTaxas);
	}

	@Override
	public void receberTransferencia(String nrContaOrigem, BigDecimal valor) {
		Operacao operacao = new Operacao(this.obterSaldo(), "Recebimento Transferência", valor);
		operacao.adicionaNrContaOperacaoTransferencia(nrContaOrigem);
		this.operacoesRealizadas.add(operacao);
		this.aumentarSaldo(valor);
	}

	@Override
	public void enviarTransferencia(String nrContaDestino, BigDecimal valor) throws SaldoInsuficienteException {
		if (valor.add(TAXA_TRANSFERENCIA).compareTo(this.obterSaldo().add(LIMITE_CONTA_PREMIUM)) > 0) {
			throw new SaldoInsuficienteException("Saldo insuficiente para transferir " + valor.toString());
		}
		
		Operacao operacao = new Operacao(this.obterSaldo(), "Transferência", valor);
		operacao.adicionaNrContaOperacaoTransferencia(nrContaDestino);
		this.operacoesRealizadas.add(operacao);
		quantidadeOperacoesNoDia++;
		this.diminuirSaldo(valor.add(TAXA_TRANSFERENCIA));
	}

	@Override
	public List<Operacao> extrato() {
		if (TAXA_EXTRATO.compareTo(this.obterSaldo().add(LIMITE_CONTA_PREMIUM)) > 0) {
			throw new RuntimeException("Saldo insuficiente para emitir extrato.");
		}
		this.diminuirSaldo(TAXA_EXTRATO);
		return this.operacoesRealizadas;
	}
	
	@Override
	public void validarQuantidadeOperacoes () throws OperacaoNaoValidadaException{
	}

	@Override
	public boolean equals(Object o) {
		return compareTo(o) == 0;
	}

	@Override
	public int hashCode() {
		if (hashCode == null) {
			hashCode = getClass().getSuperclass().getName().concat(obterNrConta()).hashCode();
		}
		return hashCode.intValue();
	}

	@Override
	public int compareTo(Object o) {
		if (this == o) return 0;
		if (o == null) return 1;
		if ((o instanceof Conta) == false) {
			return getClass().getSuperclass().getName().compareTo(o.getClass().getName());
		}
		return obterNrConta().compareTo(((Conta)o).obterNrConta());
	}
}
