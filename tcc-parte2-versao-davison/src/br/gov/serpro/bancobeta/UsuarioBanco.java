/*
Classe Dummy só para cumprir o contrato exigido pela interface do banco. Não faz sentido
o banco tratar usuário, deveria tratar contas, e o caixa eletrônico tratar usuários.
Como não é exatamente impeditivo (apenas inconveniente), cumpri os tipos exigidos pela
implemantação da parte 1. No entanto, não insisti na modelagem com problema, deixando
apenas uma classe concreta sem maiores implementações.
*/
package br.gov.serpro.bancobeta;

import br.gov.serpro.banco.Conta;
import br.gov.serpro.banco.Usuario;

public class UsuarioBanco extends Usuario {

	public UsuarioBanco(String nome, Conta conta) {
		super(nome, conta);
	}

}
