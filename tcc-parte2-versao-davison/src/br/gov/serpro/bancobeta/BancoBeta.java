package br.gov.serpro.bancobeta;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.gov.serpro.banco.BancoOperavel;
import br.gov.serpro.banco.Conta;
import br.gov.serpro.banco.Operacao;
import br.gov.serpro.banco.OperacaoNaoValidadaException;
import br.gov.serpro.banco.SaldoInsuficienteException;
import br.gov.serpro.banco.Usuario;

public class BancoBeta implements BancoOperavel {

	private static BancoBeta instanciaBanco = new BancoBeta();
	
	private Set<Conta> contas = new HashSet<Conta>();
	private HashMap<String, Conta> contaDoNumero = new HashMap<String, Conta>();

	private BancoBeta() {
	}
	
	public static BancoBeta getInstance() {
		return instanciaBanco;
	}
	
	public void adicionarConta(Conta conta) {
		if (contas.contains(conta)) {
			throw new RuntimeException("Conta " + conta.obterNrConta() + " já existe no banco Beta.");
		}
		contas.add(conta);
		contaDoNumero.put(conta.obterNrConta(), conta);
	}

	public boolean contaExistente(String numeroConta) {
		return contaDoNumero.containsKey(numeroConta);
	}
	
	public Conta obterContaComNumero(String numeroConta) {
		return contaDoNumero.get(numeroConta);
	}

	@Override
	public List<Operacao> consultarExtrato(Usuario usuario) {
		if (usuario == null) {
			throw new IllegalArgumentException("Usuário/Conta inválidos.");
		}
		if (! contaExistente(usuario.obterNrConta())) {
			throw new RuntimeException("Conta inexistente.");
		}
		return obterContaComNumero(usuario.obterNrConta()).extrato();
	}

	@Override
	public BigDecimal consultarSaldo(Usuario usuario) {
		if (usuario == null) {
			throw new IllegalArgumentException("Usuário/Conta inválidos.");
		}
		if (! contaExistente(usuario.obterNrConta())) {
			throw new RuntimeException("Conta inexistente.");
		}
		return obterContaComNumero(usuario.obterNrConta()).obterSaldo();
	}

	@Override
	public void realizarTransferencia(Usuario usuario, String nrContaDestino, BigDecimal valor)
			throws SaldoInsuficienteException {

		if (usuario == null) {
			throw new IllegalArgumentException("Usuário/Conta origem inválidos.");
		}
		if (! contaExistente(usuario.obterNrConta())) {
			throw new RuntimeException("Conta origem inexistente.");
		}
		if (nrContaDestino == null) {
			throw new IllegalArgumentException("Conta destino inválida.");
		}
		if (! contaExistente(nrContaDestino)) {
			throw new RuntimeException("Conta destino inexistente.");
		}
		if (valor == null || valor.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("Valor de transferência inválido.");
		}
		try {
			validarOperacao(usuario);
		}
		catch (OperacaoNaoValidadaException e) {
			throw new RuntimeException(e.getMessage());
		}
		obterContaComNumero(usuario.obterNrConta()).enviarTransferencia(nrContaDestino, valor);
		obterContaComNumero(nrContaDestino).receberTransferencia(usuario.obterNrConta(), valor);
	}

	@Override
	public void realizarDeposito(Usuario usuario, BigDecimal valor) {
		if (usuario == null) {
			throw new IllegalArgumentException("Usuário/Conta inválidos.");
		}
		if (! contaExistente(usuario.obterNrConta())) {
			throw new RuntimeException("Conta inexistente.");
		}
		if(valor == null || valor.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("Valor inválido para depósito.");
		}
		try {
			validarOperacao(usuario);
		}
		catch (OperacaoNaoValidadaException e) {
			throw new RuntimeException(e.getMessage());
		}
		obterContaComNumero(usuario.obterNrConta()).depositar(valor);
	}

	@Override
	public void saque(Usuario usuario, BigDecimal valor) throws SaldoInsuficienteException {
		if (usuario == null) {
			throw new IllegalArgumentException("Usuário/Conta inválidos.");
		}
		if (! contaExistente(usuario.obterNrConta())) {
			throw new RuntimeException("Conta inexistente.");
		}
		if(valor == null || valor.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("Valor inválido para saque.");
		}
		try {
			validarOperacao(usuario);
		}
		catch (OperacaoNaoValidadaException e) {
			throw new RuntimeException(e.getMessage());
		}
		obterContaComNumero(usuario.obterNrConta()).saque(valor);
	}

	@Override
	public void validarOperacao(Usuario usuario) throws OperacaoNaoValidadaException {
		if (usuario == null) {
			throw new IllegalArgumentException("Usuário/Conta inválidos.");
		}
		if (! contaExistente(usuario.obterNrConta())) {
			throw new RuntimeException("Conta inexistente.");
		}
		obterContaComNumero(usuario.obterNrConta()).validarQuantidadeOperacoes();
	}

}
