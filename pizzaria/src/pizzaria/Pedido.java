package pizzaria;

import java.util.Calendar;
import java.util.Set;

public class Pedido {
	private Set<Pizza> pizzas;
	private double distanciaEntrega;
	private DiaSemana diaSemana;

	public Pedido(Set<Pizza> pizzas, double distanciaEntrega) {
		this.pizzas = pizzas;
		this.distanciaEntrega = distanciaEntrega;
		switch (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
			case Calendar.SATURDAY: diaSemana = DiaSemana.SABADO; break;
			case Calendar.SUNDAY: diaSemana = DiaSemana.DOMINGO; break;
			case Calendar.MONDAY: diaSemana = DiaSemana.SEGUNDA; break;
			case Calendar.TUESDAY: diaSemana = DiaSemana.TERCA; break;
			case Calendar.WEDNESDAY: diaSemana = DiaSemana.QUARTA; break;
			case Calendar.THURSDAY: diaSemana = DiaSemana.QUINTA; break;
			case Calendar.FRIDAY: diaSemana = DiaSemana.SEXTA; break;
			default: break;
		}
	}
	
	public double getPreco () {
		double preco;
		preco = 0;
		for (Pizza p : pizzas) {
			preco += p.getPreco();
		}
		preco += TabelaPrecos.getValorEntrega(distanciaEntrega, diaSemana);
		return preco;
	}
	
}
