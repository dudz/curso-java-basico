package pizzaria;

import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class Testador {

	@Test
	public void testaPreco() {
		Set<Pizza> pizzas = new LinkedHashSet<Pizza>();
		
		pizzas.add(new Pizza("quatro queijos", true));
		pizzas.add(new Pizza("shimeji"));
		Pedido pedido = new Pedido(pizzas, 3.141592653);
		
		double valor;
		switch (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
		case Calendar.SATURDAY: case Calendar.FRIDAY: valor = 110.37; break;
		case Calendar.WEDNESDAY: valor = 100.0; break;
		default: valor = 109.42;
		}
		
		Assert.assertTrue(pedido.getPreco() == valor);
	}
}
