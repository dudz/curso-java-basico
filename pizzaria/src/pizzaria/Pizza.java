package pizzaria;

public class Pizza {

	private String sabor;
	private boolean bordaRecheada = false;
	
	public Pizza(String sabor) {
		this.sabor = sabor;
	}
	
	public Pizza(String sabor, boolean bordaRecheada) {
		this.sabor = sabor;
		this.bordaRecheada = bordaRecheada;
	}
	
	public double getPreco() {
		double valor;
		valor = TabelaPrecos.getValorPizza(sabor);
		valor += TabelaPrecos.getValorBordaRecheada(bordaRecheada);
		return valor;
	}
	
	public String getSabor () {
		return this.sabor;
	}
	
}
