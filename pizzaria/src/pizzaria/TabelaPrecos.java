package pizzaria;

public class TabelaPrecos {

	public static double getValorPizza(String sabor) {
		switch (sabor.toLowerCase().trim()) {
		case "queijo": case "mussarela": return 30.00;
		case "quatro queijos": return 45.00;
		case "calabresa": return 29.00;
		case "atum": return 35.00;
		case "shimeji": return 50.00;
		default: throw new IllegalArgumentException("Sabor inválido");
		}
	}

	public static double getValorBordaRecheada(boolean bordaRecheada) {
		if (! bordaRecheada) return 0;
		return 5.00;
	}
	
	public static double getValorEntrega(double distanciaKm, DiaSemana diaSemana) {
		if (distanciaKm < 2 ||
				(diaSemana == DiaSemana.QUARTA && distanciaKm <= 10.00)
		) return 0.0;
		double valor;
		valor = distanciaKm * 3;
		if (diaSemana == DiaSemana.SEXTA || diaSemana == DiaSemana.SABADO) {
			valor *= 1.1;
		}
		return Math.round(valor * 100) / 100.0;		
	}
}
