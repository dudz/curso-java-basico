package br.gov.serpro.exerciciodata;

import java.util.Date;

public class PessoaDate {

	private Date dataDeNascimento;

	public int getIdade() {
		 return getIdadeEm(new Date());
	}

	@SuppressWarnings("deprecation")
	public int getIdadeEm(Date d) {
		 int dia, mes, ano;
		 int diferenca;
		 ano = d.getYear();
		 mes = d.getMonth();
		 dia = d.getDate();
		 diferenca = ano - dataDeNascimento.getYear();
		 if (mes < dataDeNascimento.getMonth() || (mes == dataDeNascimento.getMonth()
				 && dia < this.dataDeNascimento.getDate())) diferenca--;
		 return diferenca;
	}

	
	public Date getDataDeNascimento() {
		return this.dataDeNascimento;
	}

	public void setDataDeNascimento(Date d) {
		dataDeNascimento = d;
	}
}
