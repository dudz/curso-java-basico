package br.gov.serpro.exerciciodata;

import java.time.LocalDate;
import java.time.Month;


public class Pessoa {

	private LocalDate dataDeNascimento;

	public int getIdade() {
		 return getIdadeEm(LocalDate.now());
	}

	public int getIdadeEm(LocalDate d) {
		 int dia;
		 Month mes;
		 int ano;
		 int diferenca;
		 ano = d.getYear();
		 mes = d.getMonth();
		 dia = d.getDayOfMonth();
		 diferenca = ano - dataDeNascimento.getYear();
		 if (mes.compareTo(dataDeNascimento.getMonth()) < 0 || (mes == dataDeNascimento.getMonth()
				 && dia < this.dataDeNascimento.getDayOfMonth())) diferenca--;
		 return diferenca;
	}

	
	public LocalDate getDataDeNascimento() {
		return this.dataDeNascimento;
	}

	public void setDataDeNascimento(LocalDate d) {
		dataDeNascimento = d;
	}
}
