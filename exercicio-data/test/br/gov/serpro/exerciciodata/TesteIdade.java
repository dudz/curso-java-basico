package br.gov.serpro.exerciciodata;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Ignore;
import org.junit.Test;

public class TesteIdade {

	@Test
	public void testaIdade() {
		Pessoa p = new Pessoa();
		p.setDataDeNascimento(LocalDate.of(1978, Month.SEPTEMBER, 13));
		LocalDate d = LocalDate.now();
		assertEquals(
			p.getIdade(),
			d.getYear() - 1978 -
			(d.getMonthValue() < 9 || (d.getMonthValue() == 9 && d.getDayOfMonth() < 13) ? 1 : 0)
		);
	}

	@Test
	public void testaIdadeAno2050() {
		Pessoa p = new Pessoa();
		p.setDataDeNascimento(LocalDate.of(1978, 9, 13));
		LocalDate d = LocalDate.of(2050, 1, 1);
		assertEquals(p.getIdadeEm(d), 71);
	}
	
	@Test
	@Ignore
	public void testeBissextoIncompleto() {
		Pessoa p = new Pessoa();
		p.setDataDeNascimento(LocalDate.of(1500, Month.FEBRUARY, 29));
		LocalDate d = LocalDate.of(1501, Month.FEBRUARY, 28);
		assertEquals(p.getIdadeEm(d), 0);
	}

	@Test
	@Ignore
	public void testeBissextoCompleto() {
		Pessoa p = new Pessoa();
		p.setDataDeNascimento(LocalDate.of(1500, 2, 29));
		LocalDate d = LocalDate.of(1501, 3, 1);
		assertEquals(p.getIdadeEm(d), 1);
	}
	
	@Test
	public void testeGetData () {
		Pessoa p = new Pessoa();
		p.setDataDeNascimento(LocalDate.of(1978, 9, 13));
		assertEquals(LocalDate.of(1978, Month.SEPTEMBER, 13), p.getDataDeNascimento());
	}
}
