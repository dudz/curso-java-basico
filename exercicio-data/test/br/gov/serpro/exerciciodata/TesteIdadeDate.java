package br.gov.serpro.exerciciodata;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import org.junit.Test;

public class TesteIdadeDate {

	@SuppressWarnings("deprecation")
	@Test
	public void testaIdade() {
		PessoaDate p = new PessoaDate();
		p.setDataDeNascimento(new Date(1978 - 1900, 9 - 1, 13));
		Date d = new Date();
		assertEquals(
			p.getIdade(),
			d.getYear() - (1978 - 1900) -
			(d.getMonth() < (9 - 1) || (d.getMonth() == (9 - 1) && d.getDate() < 13) ? 1 : 0)
		);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testaIdadeAno2050() {
		PessoaDate p = new PessoaDate();
		p.setDataDeNascimento(new Date(1978 - 1900, 9 - 1, 13));
		Date d = new Date(2050 - 1900, 1 - 1, 1);
		assertEquals(p.getIdadeEm(d), 71);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testeBissextoIncompleto() {
		PessoaDate p = new PessoaDate();
		p.setDataDeNascimento(new Date(1500 - 1900, 2 - 1, 29));
		Date d = new Date(1501 - 1900, 2 - 1, 28);
		assertEquals(p.getIdadeEm(d), 0);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testeBissextoCompleto() {
		PessoaDate p = new PessoaDate();
		p.setDataDeNascimento(new Date(1500 - 1900, 2 - 1, 29));
		Date d = new Date(1501 - 1900, 3 - 1, 1);
		assertEquals(p.getIdadeEm(d), 1);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testeGetData () {
		PessoaDate p = new PessoaDate();
		p.setDataDeNascimento(new Date(1978 - 1900, 9 - 1, 13));
		assertEquals(new Date(1978 - 1900, 9 - 1, 13), p.getDataDeNascimento());
	}
}
