import java.util.ArrayList;
import java.util.List;

public class Cofrinho {

	private List<Moeda> moedas;
	
	public Cofrinho() {
		moedas = new ArrayList<Moeda>();
	}

	public void receberMoeda(Moeda moeda) {
		moedas.add(moeda);
	}	
	
	public int quantidadeMoedasPorValor (ValorMoeda valorMoeda) {
		int qtd = 0;
		for (Moeda m : moedas) {
			if (m.getValorMoeda() == valorMoeda) qtd++;
		}
		return qtd;
	}
	
	public ValorMoeda maiorValor() {
		ValorMoeda maiorValor = null;
		for (Moeda m : moedas) {
			if (maiorValor == null) {
				maiorValor = m.getValorMoeda();
				continue;
			}
			else if (m.getValorMoeda().getValor() > maiorValor.getValor()) {
				maiorValor = m.getValorMoeda();
			}
		}
		return maiorValor;
	}
	
	public int quantidadeMoedas() {
		return moedas.size();
	}
}
