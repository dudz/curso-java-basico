
public enum ValorMoeda {

	UM_CENTAVO(0.01),
	CINCO_CENTAVOS(0.05),
	DEZ_CENTAVOS(0.10),
	VINTE_CINCO_CENTAVOS(0.25),
	CINQUENTA_CENTAVOS(0.50),
	UM_REAL(1.00)
	;
	
	private final double valor;
	
	ValorMoeda(double valor) {
		this.valor = valor;
	}
	
	public double getValor() {
		return this.valor;
	}
}
