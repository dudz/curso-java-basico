
public class Moeda {

	private ValorMoeda valorMoeda;
	
	public Moeda(ValorMoeda valorMoeda) {
		this.valorMoeda = valorMoeda;
	}
	
	public ValorMoeda getValorMoeda() {
		return this.valorMoeda;
	}
	
}
