
public class TestadorCofrinho {

	public static void main(String[] args) {
		Cofrinho cofrinho = new Cofrinho();
		
		cofrinho.receberMoeda(new Moeda(ValorMoeda.CINCO_CENTAVOS));
		cofrinho.receberMoeda(new Moeda(ValorMoeda.UM_REAL));
		cofrinho.receberMoeda(new Moeda(ValorMoeda.CINCO_CENTAVOS));
		cofrinho.receberMoeda(new Moeda(ValorMoeda.CINCO_CENTAVOS));
		cofrinho.receberMoeda(new Moeda(ValorMoeda.CINQUENTA_CENTAVOS));
		cofrinho.receberMoeda(new Moeda(ValorMoeda.CINCO_CENTAVOS));

		System.out.println("Maior valor: " + cofrinho.maiorValor().getValor());
		
		for (ValorMoeda v : ValorMoeda.values()) {
			System.out.println("Quantidade valor "+v.getValor()+": " + cofrinho.quantidadeMoedasPorValor(v));
		}
		
		System.out.println("Quantidade total de moedas: " + cofrinho.quantidadeMoedas());
		
	}

}
