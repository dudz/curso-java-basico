package br.gov.serpro.pilha;

public class Pilha {
	private Object[] elementos;
	private int topo = 0;

	
	public Pilha(int maximo) {
		if (maximo < 1) {
			throw new IllegalArgumentException("Tamanho de pilha invalido.");
		}
		elementos = new Object[maximo];
	}

	public void empilhar(Object elemento) {
		if (topo >= elementos.length) {
			throw new RuntimeException("Estouro de pilha");
		}
		elementos[topo] = elemento;
		topo++;
	}

	public Object  desempilhar() {
		if (topo < 1) {
			throw new RuntimeException("Tentativa de tirar elemento de pilha vazia.");
		}
		Object retorno;
		topo--;
		retorno = elementos[topo];
		elementos[topo] = null;
		return retorno;
	}
	
	public Object topo() {
		return elementos[topo - 1];
	}
	
	public int tamanho() {
		return topo;
	}
}
