package br.gov.serpro.curso;

public class Carro {
	int potencia;
	private int velocidade;
	String nome;
	
	
	void acelerar() {
		velocidade += potencia;
	}
	
	void frear() {
		velocidade /= 2;
	}
	
	int getVelocidade() {
		return velocidade;
	}
	
	void imprimir() {
		System.out.println("O carro " + nome + " está a velocidade " + getVelocidade() + " km/h.");
	}
	
	int comparaVelocidade (Carro outro) {
		return getVelocidade() - outro.getVelocidade();
	}
	
	void setVelocidade(int velocidade) {
		this.velocidade = velocidade;
	}
	
}
