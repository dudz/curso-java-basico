package br.gov.serpro.curso;

public class Principal {

	public static void main(String[] args) {
		Carro corcel = new Carro();
		Carro carango = new Carro();
		
		corcel.potencia = 10;
		corcel.nome = "Corcel";
		corcel.setVelocidade(0);
		
		carango.potencia = 15;
		carango.nome = "Carango";
		carango.setVelocidade(0);

		corcel.acelerar();
		corcel.acelerar();
		corcel.acelerar();
		corcel.acelerar();
		corcel.acelerar();
		corcel.frear();
		
		carango.acelerar();
		
		corcel.imprimir();
		carango.imprimir();
	}

}
