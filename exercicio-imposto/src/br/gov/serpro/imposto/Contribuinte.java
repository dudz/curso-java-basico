package br.gov.serpro.imposto;

public abstract class Contribuinte {
	private String nome;
	private double renda;

	public String getNome() {
		return nome;
	}
	
	public double getRenda() {
		return renda;
	}
	
	protected Contribuinte(String nome, double renda) {
		this.nome = nome;
		this.renda = renda;
	}
	
	public abstract String getNi();
	public abstract double calcularImposto();
}
