package br.gov.serpro.imposto;

public class PessoaFisica extends Contribuinte {
	private String cpf;

	public PessoaFisica(String nome, double renda, String ni) {
		super(nome, renda);
		this.cpf = ni;
	}

	public String getCpf() {
		return cpf;
	}
	
	@Override
	public String getNi() {
		return cpf;
	}

	@Override
	public double calcularImposto() {
		double renda = this.getRenda();
		double al;
		if (renda <= 2000.0) al = 0.0; 
		else if (renda <= 4000.0) al = 0.10; 
		else if (renda <= 6000.0) al = 0.15; 
		else if (renda <= 8000.0) al = 0.25; 
		else al = 0.30; 
		return al * renda;
	}
	
}
