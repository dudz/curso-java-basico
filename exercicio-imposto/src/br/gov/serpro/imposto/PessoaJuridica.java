package br.gov.serpro.imposto;

public class PessoaJuridica extends Contribuinte {

	private String cnpj; 
	public PessoaJuridica(String nome, double renda, String cnpj) {
		super(nome, renda);
		this.cnpj = cnpj;
	}
	
	@Override
	public String getNi() {
		return cnpj;
	}

	@Override
	public double calcularImposto() {
		return this.getRenda() * 0.1;
	}
}
