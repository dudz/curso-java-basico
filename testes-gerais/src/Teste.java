import java.util.Objects;

public class Teste {

	private int x = 0;
	public static void main(String[] args) {

		Teste t1 = new Teste(1);
		Teste t2 = new Teste(2);
		System.out.println("t1.x antes = " + t1.getX());
		System.out.println("t2.x antes = " + t2.getX());
		t1.mudarOOutro(t2, 123);
		System.out.println("t1.x depois = " + t1.getX());
		System.out.println("t2.x depois = " + t2.getX());
		t1.rodarMetodoDoOutro(t2);
		
		System.out.println(t1.equals(t2));
	}
	public void rodarMetodoDoOutro(Teste outro) {
		outro.metodoPrivado();
	}
	public Teste(int n) {
		x = n;
	}
	public void mudarOOutro(Teste outro, int valor) {
		outro.x = valor;
	}
	public int getX() {
		return x;
	}
	private void metodoPrivado() {
		System.out.println("meu valor e' " + x);
	}
	
	@Override
	public boolean equals(Object o) {
		return Objects.equals(this, o);
	}
}
