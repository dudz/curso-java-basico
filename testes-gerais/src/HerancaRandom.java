import java.util.Random;

public class HerancaRandom extends Random {

	private static final long serialVersionUID = 1L;

	private Integer x = null;
	
	public HerancaRandom() {
		//super();
		x = Integer.valueOf(123);
	}
	
	@Override
	public void setSeed(long a) {
		System.out.println("Rodando seed de HerancaRandom.");
		System.out.println("Valor de x e'' ");
		System.out.println(x.intValue());
	}
	
	@Override
	protected int next(int n) {
		return x.intValue();
	}

	public static void main(String[] args) {
		Random x = new HerancaRandom();
		System.out.println(x.nextInt());
	}
}
