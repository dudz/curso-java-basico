import org.junit.Test;
import org.junit.Assert;


public class TesteCompra {

	
	public static void main(String[] args) {
		new TesteCompra().compraAVista();
		new TesteCompra().compraParcelada();
		System.out.println("fim");
	}
	
	@Test
	public void compraAVista() {
		Compra compra = new Compra(500);

		Assert.assertEquals (1, compra.getNumeroParcelas());
		Assert.assertEquals (500, compra.getValorTotal());
		Assert.assertEquals (500, compra.getValorParcela());
		
	}
	
	@Test
	public void compraParcelada() {
		Compra compra = new Compra(4, 250);
		
		Assert.assertEquals (4, compra.getNumeroParcelas());
		Assert.assertEquals (1000, compra.getValorTotal());
		Assert.assertEquals (250, compra.getValorParcela());
		
	}
}
