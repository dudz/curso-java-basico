
public class Compra {

	private int valorTotal;
	private int numeroParcelas;
	

	
	// à vista
	public Compra(int valor) {
		this.valorTotal = valor;
		this.numeroParcelas = 1;
	}
	
	//parcelado
	public Compra(int qtdParcelas, int valorParcela) {
		this.numeroParcelas = qtdParcelas;
		this.valorTotal = valorParcela * qtdParcelas;
	}
	
	public int getValorTotal() {
		return this.valorTotal;
	}
	public int getNumeroParcelas() {
		return this.numeroParcelas;
	}
	public int getValorParcela() {
		return this.valorTotal / this.numeroParcelas;
	}
}
