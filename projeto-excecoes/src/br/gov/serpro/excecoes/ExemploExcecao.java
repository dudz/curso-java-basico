package br.gov.serpro.excecoes;

public class ExemploExcecao {

	public double porcao1(double valor, double porCento) {
		if (porCento > 100 || porCento < 0) {
			return -1;
		}
		return valor * porCento / 100;
	}

	public double porcao(double valor, double porCento) throws ForaDoIntervaloException {
		if (porCento > 100 || porCento < 0) {
			throw new ForaDoIntervaloException(String.valueOf(porCento));
		}
		return valor * porCento / 100;
	}
}
