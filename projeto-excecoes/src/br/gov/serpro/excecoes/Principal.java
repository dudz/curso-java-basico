package br.gov.serpro.excecoes;

public class Principal {

	public static void main(String[] args) {
		a(-100);
		System.out.println();
		System.out.println("terminando main");
	}

	public static void a(int i) {
		System.out.println("Executando a() com " + i);
		try {
			b(i);
		} catch (Exception exception) {
			System.out.println("tratando...");
		}
	}
	
	public static void b(int i) throws Exception {
		System.out.println("Executando b() com " + i);
		if (i > 0)
			throw new Exception("mensagem");
		System.out.println("terminando b");
	}
	
}
