package br.gov.serpro.excecoes;

public class ForaDoIntervaloException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ForaDoIntervaloException(String message) {
		super("Valor fora do intervalo: " + message);
	}

}
