package br.gov.serpro.streetfighter;

public class Ken extends Lutador implements Radouken, Roriuken {

	public Ken() {
		super("Ken");
	}
	
	@Override
	public void efetuaRadouken() {
		System.out.println("Ken efetua Radouken");
	}

	@Override
	public void efetuaRoriuken() {
		System.out.println("Ken efetua Roriken com fogo");
	}

}
