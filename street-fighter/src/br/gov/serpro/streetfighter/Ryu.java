package br.gov.serpro.streetfighter;

public class Ryu extends Lutador implements Radouken, Roriuken {

	public Ryu() {
		super("Ryu");
	}
	
	@Override
	public void efetuaRadouken() {
		System.out.println("Ryu efetua Radouken");
	}

	@Override
	public void efetuaRoriuken() {
		System.out.println("Ryu efetua Roriken");
	}

}
