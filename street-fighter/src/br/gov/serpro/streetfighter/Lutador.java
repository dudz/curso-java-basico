package br.gov.serpro.streetfighter;

public class Lutador {
	private String nome;
	
	public Lutador(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
}
