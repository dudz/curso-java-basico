package br.gov.serpro.tv;

public class Musica implements ItemGrade {

	private int numero;
	private String nome;
	
	public Musica(int numeroMusica, String nome) {
		numero = numeroMusica;
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Musica " + numero + ": " + nome;
	}
	
	@Override
	public int getNumero() {
		return numero;
	}
	
	@Override
	public String getNome() {
		return nome;
	}
}
