package br.gov.serpro.tv;

public class ControleRemoto {

	private Controlavel aparelho;
	
	public ControleRemoto (Controlavel aparelho) {
		if (aparelho == null) {
			throw new IllegalArgumentException("Controlavel inválido em ControleRemoto(aparelho).");
		}
		this.aparelho = aparelho;
	}
	
	public void parear (Controlavel aparelho) {
		if (aparelho == null) {
			throw new IllegalArgumentException("Erro ao parear controle remoto com Controlavel.");
		}
		this.aparelho = aparelho;
	}
	
	public ItemGrade proximo() {
		return aparelho.proximo();
	}
	
	public ItemGrade anterior() {
		return aparelho.anterior();
	}
	
	public ItemGrade mudar (int numero) {
		return aparelho.muda(numero);
	}
	
	public boolean ligaDesliga() {
		if (aparelho.isLigado()) {
			aparelho.desliga();
		} else {
			aparelho.liga();
		}
		return aparelho.isLigado();
	}
	
	public int aumentaVolume () {
		return aparelho.aumentaVolume();
	}

	public int diminuiVolume () {
		return aparelho.diminuiVolume();
	}
}

