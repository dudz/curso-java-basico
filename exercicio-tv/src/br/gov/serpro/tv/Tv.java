package br.gov.serpro.tv;

public class Tv implements Controlavel {
	
	private static final int VOLUME_PADRAO = 10;
	public static final int VOLUME_MAXIMO = 50;
	
	private Canal[] canaisPossiveis;
	private int canalAtivo = 0;
	private int volume = VOLUME_PADRAO;
	private boolean ligada = false;

	public Tv(Canal[] canaisPossiveis) {
		if (canaisPossiveis == null || canaisPossiveis.length < 1) {
			throw new IllegalArgumentException("Erro ao contruir TV.");
		}
		this.canaisPossiveis = canaisPossiveis;
	}
	
	@Override
	public void liga() {
		ligada = true;
	}
	
	@Override
	public void desliga() {
		ligada = false;
	}
	
	/**
	 * Vai para o próximo canal disponível (de forma circular).
	 * @return O canal ativo.
	 */
	@Override
	public Canal proximo() {
		if (!ligada) return null;
		if (canaisPossiveis.length > 1) {
			canalAtivo = (canalAtivo + 1) % canaisPossiveis.length;
		}
		return getCanalAtivo();
	}
	
	/**
	 * Vai para o canal disponível anterior (de forma circular).
	 * @return O canal ativo.
	 */
	@Override
	public Canal anterior() {
		if (!ligada) return null;
		if (canaisPossiveis.length > 1) {
			canalAtivo = (canalAtivo + canaisPossiveis.length - 1) % canaisPossiveis.length;
		}
		return getCanalAtivo();
	}
	
	/**
	 * Vai para o canal de número desejado, caso exista.
	 * @param numeroCanal Número do canal desejado.
	 * @return O canal ativo após mudança.
	 */
	@Override
	public Canal muda (int numeroCanal) {
		if (!ligada) return null;
		for (int i = 0; i < canaisPossiveis.length; i++) {
			if (canaisPossiveis[i].getNumero() == numeroCanal) {
				this.canalAtivo = i;
				break;
			}
		}
		return canaisPossiveis[canalAtivo];
	}
	
	public Canal getCanalAtivo() {
		if (!ligada) return null;
		return canaisPossiveis[canalAtivo];
	}
	
	/**
	 * Aumenta volume até o maior possível.
	 * @return Novo volume.
	 */
	public int aumentaVolume() {
		if (!ligada) return 0;
		if (volume < VOLUME_MAXIMO) {
			volume++;
		}
		return volume;
	}
	
	/**
	 * Diminui volume até o menor possível.
	 * @return Novo volume.
	 */
	public int diminuiVolume() {
		if (!ligada) return 0;
		if (volume > 0) {
			volume--;
		}
		return volume;
	}
	
	@Override
	public boolean isLigado() {
		return ligada;
	}
	
}
