package br.gov.serpro.tv;

public class Canal implements ItemGrade {

	private int numero;
	private String nome;
	
	public Canal(int numeroCanal, String nome) {
		numero = numeroCanal;
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Canal " + numero + ": " + nome;
	}
	
	@Override
	public int getNumero() {
		return numero;
	}
	
	@Override
	public String getNome() {
		return nome;
	}
}
