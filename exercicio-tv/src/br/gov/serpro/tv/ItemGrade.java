package br.gov.serpro.tv;

public interface ItemGrade {
	public abstract String getNome();
	public abstract int getNumero();
}
