package br.gov.serpro.tv;

public interface Controlavel {

	public abstract ItemGrade proximo();
	public abstract ItemGrade anterior();
	public abstract ItemGrade muda(int numero);
	public abstract int diminuiVolume();
	public abstract int aumentaVolume();
	public abstract boolean isLigado();
	public abstract void liga();
	public abstract void desliga();
}
