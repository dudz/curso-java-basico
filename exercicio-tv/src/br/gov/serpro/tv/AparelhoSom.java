package br.gov.serpro.tv;

public class AparelhoSom implements Controlavel {
	
	private static final int VOLUME_PADRAO = 10;
	public static final int VOLUME_MAXIMO = 50;
	
	private Musica[] musicas;
	private int musicaAtiva = 0;
	private int volume = VOLUME_PADRAO;
	private boolean ligada = false;

	public AparelhoSom(Musica[] musicas) {
		if (musicas == null) {
			throw new IllegalArgumentException("Erro ao contruir AparelhoSom.");
		}
		this.musicas = musicas;
	}
	
	@Override
	public void liga() {
		ligada = true;
	}
	
	@Override
	public void desliga() {
		ligada = false;
	}
	
	/**
	 * Vai para a próxima música disponível (de forma circular).
	 * @return A música ativo.
	 */
	@Override
	public Musica proximo() {
		if (!ligada) return null;
		if (musicas.length > 1) {
			musicaAtiva = (musicaAtiva + 1) % musicas.length;
		}
		return getMusicaAtiva();
	}
	
	/**
	 * Vai para a música disponível anterior (de forma circular).
	 * @return A música ativo.
	 */
	@Override
	public Musica anterior() {
		if (!ligada) return null;
		if (musicas.length > 1) {
			musicaAtiva = (musicaAtiva + musicas.length - 1) % musicas.length;
		}
		return getMusicaAtiva();
	}
	
	/**
	 * Vai para a música de número desejado, caso exista.
	 * @param numeroMusica Número da música desejada.
	 * @return A música ativo após mudança.
	 */
	@Override
	public Musica muda (int numeroMusica) {
		if (!ligada) return null;
		for (int i = 0; i < musicas.length; i++) {
			if (musicas[i].getNumero() == numeroMusica) {
				this.musicaAtiva = i;
				break;
			}
		}
		return musicas[musicaAtiva];
	}
	
	public Musica getMusicaAtiva() {
		if (!ligada) return null;
		return musicas[musicaAtiva];
	}
	
	/**
	 * Aumenta volume até o maior possível.
	 * @return Novo volume.
	 */
	public int aumentaVolume() {
		if (!ligada) return 0;
		if (volume < VOLUME_MAXIMO) {
			volume++;
		}
		return volume;
	}
	
	/**
	 * Diminui volume até o menor possível.
	 * @return Novo volume.
	 */
	public int diminuiVolume() {
		if (!ligada) return 0;
		if (volume > 0) {
			volume--;
		}
		return volume;
	}
	
	@Override
	public boolean isLigado() {
		return ligada;
	}
	
}
