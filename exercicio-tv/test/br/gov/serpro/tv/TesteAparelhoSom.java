package br.gov.serpro.tv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TesteAparelhoSom {

	private static AparelhoSom aparelhoSom;
	
	@Before
	public void iniciarTestes() {
		Musica[] musicas = new Musica[] {
			new Musica(2, "Musica2"),
			new Musica(4, "Musica4"),
			new Musica(5, "Musica5"),
			new Musica(7, "Musica7"),
			new Musica(9, "Musica9"),
			new Musica(11, "Musica11"),
			new Musica(13, "Musica13"),
			new Musica(21, "Musica21")
		};
		aparelhoSom = new AparelhoSom(musicas);
	}
	
	@Test
	public void aparelhoDesligadoNaoDeveFazerNada() {
		aparelhoSom.desliga();
		assertNull(aparelhoSom.proximo());
		assertEquals(aparelhoSom.aumentaVolume(), 0);
		assertNull(aparelhoSom.anterior());
		assertEquals(aparelhoSom.diminuiVolume(), 0);
		assertNull(aparelhoSom.getMusicaAtiva());
		assertFalse(aparelhoSom.isLigado());
		assertNull(aparelhoSom.muda(5));
	}
	
	@Test
	public void aumentaMusica() {
		ControleRemoto controle = new ControleRemoto(aparelhoSom);
		aparelhoSom.liga();
		assertEquals(aparelhoSom.getMusicaAtiva().getNumero(), 2);
		controle.proximo();
		assertEquals(aparelhoSom.getMusicaAtiva().getNumero(), 4);
		controle.proximo();
		controle.proximo();
		assertEquals(aparelhoSom.getMusicaAtiva().getNumero(), 7);
		controle.proximo();
		controle.proximo();
		controle.proximo();
		controle.proximo();
		controle.proximo();
		assertEquals(aparelhoSom.getMusicaAtiva().getNumero(), 2);
	}
	
	@Test
	public void diminuiMusica() {
		ControleRemoto controle = new ControleRemoto(aparelhoSom);
		aparelhoSom.liga();
		controle.mudar(7);
		controle.anterior();
		assertEquals(aparelhoSom.getMusicaAtiva().getNumero(), 5);
	}
	
	@Test
	public void volumeMaximoDeveSer50() {
		ControleRemoto controle = new ControleRemoto(aparelhoSom);
		aparelhoSom.liga();
		for (int i = 0; i < 100; i++) controle.aumentaVolume();
		assertEquals(controle.aumentaVolume(), 50);
	}
	
	@Test
	public void volumeMinimoDeveSer0() {
		ControleRemoto controle = new ControleRemoto(aparelhoSom);
		aparelhoSom.liga();
		for (int i = 0; i < 100; i++) controle.diminuiVolume();
		assertEquals(controle.diminuiVolume(), 0);
	}
	
	@Test
	public void ligaDesligaDeveFuncionar() {
		ControleRemoto controle = new ControleRemoto(aparelhoSom);
		aparelhoSom.desliga();
		assertFalse(aparelhoSom.isLigado());
		controle.ligaDesliga();
		assertTrue(aparelhoSom.isLigado());
		controle.ligaDesliga();
		assertFalse(aparelhoSom.isLigado());
	}
	
	@Test
	public void mudarMusicaControleRemoto() {
		ControleRemoto controle = new ControleRemoto(aparelhoSom);
		aparelhoSom.liga();
		controle.mudar(4);
		assertEquals(aparelhoSom.getMusicaAtiva().getNumero(), 4);
		controle.mudar(13);
		assertEquals(aparelhoSom.getMusicaAtiva().getNumero(), 13);
	}
	
	@Test
	public void musicaDeveSer5() {
		ControleRemoto controle = new ControleRemoto(aparelhoSom);
		aparelhoSom.liga();
		controle.mudar(5);
		assertEquals(aparelhoSom.getMusicaAtiva().getNome(), "Musica5");
		assertEquals(aparelhoSom.getMusicaAtiva().toString(), "Musica 5: Musica5");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void deveDarExcecaoControleRemoto() {
		new ControleRemoto(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void deveDarExcecaoConstrutorAparelhoSom() {
		new AparelhoSom(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void deveDarExcecaoParearAparelhoSom() {
		ControleRemoto controle = new ControleRemoto(aparelhoSom);
		controle.parear(null);
	}

	@Test
	public void deveParearComOutroAparelho() {
		AparelhoSom novoAparelho = new AparelhoSom(
				new Musica[] {new Musica(2, "Musica2"), new Musica(4, "Musica4")});
		ControleRemoto controle = new ControleRemoto(aparelhoSom);
		novoAparelho.liga();
		controle.parear(novoAparelho);
		assertEquals(controle.proximo().getNumero(), 4);
	}

	@Test
	public void devePermanecerNaMusicaUnica() {
		AparelhoSom novoAparelho = new AparelhoSom(new Musica[] {new Musica(2, "Musica2")});
		novoAparelho.liga();
		ControleRemoto controle = new ControleRemoto(novoAparelho);
		assertEquals(controle.proximo().getNumero(), 2);
	}
	
}
