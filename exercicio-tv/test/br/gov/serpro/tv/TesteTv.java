package br.gov.serpro.tv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TesteTv {

	private static Tv tv;
	
	@Before
	public void iniciarTestes() {
		Canal[] canais = new Canal[] {
			new Canal(2, "Cultura"),
			new Canal(4, "SBT"),
			new Canal(5, "Globo"),
			new Canal(7, "Record"),
			new Canal(9, "Rede TV"),
			new Canal(11, "Gazeta"),
			new Canal(13, "Bandeirantes"),
			new Canal(21, "Canal 21")
		};
		tv = new Tv(canais);
	}
	
	@Test
	public void tvDesligadaNaoDeveFazerNada() {
		tv.desliga();
		assertNull(tv.proximo());
		assertEquals(tv.aumentaVolume(), 0);
		assertNull(tv.anterior());
		assertEquals(tv.diminuiVolume(), 0);
		assertNull(tv.getCanalAtivo());
		assertFalse(tv.isLigado());
		assertNull(tv.muda(5));
	}
	
	@Test
	public void aumentaCanal() {
		ControleRemoto controle = new ControleRemoto(tv);
		tv.liga();
		assertEquals(tv.getCanalAtivo().getNumero(), 2);
		controle.proximo();
		assertEquals(tv.getCanalAtivo().getNumero(), 4);
		controle.proximo();
		controle.proximo();
		assertEquals(tv.getCanalAtivo().getNumero(), 7);
		controle.proximo();
		controle.proximo();
		controle.proximo();
		controle.proximo();
		controle.proximo();
		assertEquals(tv.getCanalAtivo().getNumero(), 2);
	}
	
	@Test
	public void diminuiCanal() {
		ControleRemoto controle = new ControleRemoto(tv);
		tv.liga();
		controle.mudar(7);
		controle.anterior();
		assertEquals(tv.getCanalAtivo().getNumero(), 5);
	}
	
	@Test
	public void volumeMaximoDeveSer50() {
		ControleRemoto controle = new ControleRemoto(tv);
		tv.liga();
		for (int i = 0; i < 100; i++) controle.aumentaVolume();
		assertEquals(controle.aumentaVolume(), 50);
	}
	
	@Test
	public void volumeMinimoDeveSer0() {
		ControleRemoto controle = new ControleRemoto(tv);
		tv.liga();
		for (int i = 0; i < 100; i++) controle.diminuiVolume();
		assertEquals(controle.diminuiVolume(), 0);
	}
	
	@Test
	public void ligaDesligaDeveFuncionar() {
		ControleRemoto controle = new ControleRemoto(tv);
		tv.desliga();
		assertFalse(tv.isLigado());
		controle.ligaDesliga();
		assertTrue(tv.isLigado());
		controle.ligaDesliga();
		assertFalse(tv.isLigado());
	}
	
	@Test
	public void mudarCanalControleRemoto() {
		ControleRemoto controle = new ControleRemoto(tv);
		tv.liga();
		controle.mudar(4);
		assertEquals(tv.getCanalAtivo().getNumero(), 4);
		controle.mudar(13);
		assertEquals(tv.getCanalAtivo().getNumero(), 13);
	}
	
	@Test
	public void canalDeveSerGlobo() {
		ControleRemoto controle = new ControleRemoto(tv);
		tv.liga();
		controle.mudar(5);
		assertEquals(tv.getCanalAtivo().getNome(), "Globo");
		assertEquals(tv.getCanalAtivo().toString(), "Canal 5: Globo");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void deveDarExcecaoControleRemoto() {
		new ControleRemoto(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void deveDarExcecaoConstrutorTv() {
		new Tv(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void deveDarExcecaoParearTv() {
		ControleRemoto controle = new ControleRemoto(tv);
		controle.parear(null);
	}

	@Test
	public void deveParearComOutraTv() {
		Tv novaTv = new Tv(new Canal[] {new Canal(2, "Cultura"), new Canal(4, "SBT")});
		ControleRemoto controle = new ControleRemoto(tv);
		novaTv.liga();
		controle.parear(novaTv);
		assertEquals(controle.proximo().getNumero(), 4);
	}

	@Test
	public void devePermanecerNoCanalUnico() {
		Tv novaTv = new Tv(new Canal[] {new Canal(2, "Cultura")});
		novaTv.liga();
		ControleRemoto controle = new ControleRemoto(novaTv);
		assertEquals(controle.proximo().getNumero(), 2);
	}
	
}
