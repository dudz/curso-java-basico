public class UtilEstatico {
	
	private static long randomSeed = -1;

	public static double fahrenheitToCelsius (double grauFahrenheit) {
		return (grauFahrenheit - 32.0) * 5.0 / 9.0;		
	}
	public static double celsiusToFahrenheit (double grauCelcius) {
		return grauCelcius * 9.0 / 5.0 + 32.0;		
	}
	
	public static double max(double a, double b) {
		return a >= b ? a : b;
	}
	public static int max(int a, int b) {
		return a >= b ? a : b;
	}
	
	public synchronized static int inteiroAleatorio(int limiteInferior, int limiteSuperior) {
		if (randomSeed < 0) randomSeed = System.currentTimeMillis();
		randomSeed = (randomSeed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL;
		double rnd = (randomSeed >>> 16) / 4294967296.0;
		return limiteInferior + (int)((limiteSuperior - limiteInferior + 1) * rnd);
	}
	
	public static boolean cpfValido (String cpf) {
		if (cpf.matches("^[0-9]{11}$") == false) return false;
		long cpfLong = Long.parseLong(cpf, 10);
		long aux;
		int soma = 0;
		aux = cpfLong / 100;
		if (aux == 0) return false;
		for (int i = 2; i <= 10; i++) {
			soma += (aux % 10) * i;
			aux /= 10;
		}
		if (soma * 10 % 11 % 10 != cpfLong / 10 % 10) return false;
		aux = cpfLong / 10;
		soma = 0;
		for (int i = 2; i <= 11; i++) {
			soma += (aux % 10) * i;
			aux /= 10;
		}
		if (soma * 10 % 11 % 10 != cpfLong % 10) return false;
		return true;
	}

	public static boolean cpfValido (long cpf) {
		String cpfStr;
		if (cpf < 10000000000L) {
			cpfStr = "00000000000".concat(String.valueOf(cpf));
			cpfStr = cpfStr.substring(cpfStr.length() - 11);
		}
		else cpfStr = String.valueOf(cpf);
		return cpfValido(cpfStr);
	}
	public static String cpfFormatado (String cpf) {
		cpf = "00000000000".concat(cpf.replaceAll("[^0-9]", ""));
		cpf = cpf.substring(cpf.length() - 11);
		return cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-" +
			cpf.substring(9)
		;
	}
	public static String cpfFormatado (long cpf) {
		return cpfFormatado(String.valueOf(cpf));
	}

	public static double raizQuadrada (double x) {
		if (x < 0.0) return Double.NaN;
		if (x == 0.0) return 0.0;
		double a, aa;
		a = x / 2.0;
		do {
			aa = a;
			a = (x / a) / 2.0 + a / 2.0;
		} while (a != aa);
		return a;
	}
}
