

public class Testador {

	
	public static void main(String[] args) {
		
		System.out.println(UtilEstatico.fahrenheitToCelsius(100));
		System.out.println(UtilEstatico.celsiusToFahrenheit(25));
		
		System.out.println(UtilEstatico.max(123.1, 456.1));
		System.out.println(UtilEstatico.max(456.1, 123.1));
		System.out.println(UtilEstatico.max(123, 456));
		System.out.println(UtilEstatico.max(456, 123));
		
		for (int i = 0; i < 5; i++) {
			System.out.print(UtilEstatico.inteiroAleatorio(314, 1592) + " ");
		}
		System.out.println();
		
		System.out.println(UtilEstatico.cpfValido(0));
		System.out.println(UtilEstatico.cpfValido("27780785840"));
		System.out.println(UtilEstatico.cpfValido(27780785840L));
		System.out.println(UtilEstatico.cpfValido(30723401888L));
		
		System.out.println(UtilEstatico.cpfFormatado(27780785840L));
		System.out.println(UtilEstatico.cpfFormatado("27780785840"));
		
		System.out.println(UtilEstatico.raizQuadrada(Math.PI));
	}
	
	
}
